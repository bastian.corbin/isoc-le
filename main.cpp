#include <iostream>

int main(int argc, char **argv)
{
  if(argc == 2)
  {
    int l = atoi(argv[1]);

    if(l >= 1)
    {
      int sc = l - 1;
      int n = 1;

      for(int y = 0; y < l; y++)
      {
        int x = 0;

        while(x < sc)
        {
          std::cout << " ";
          x++;
        }

        for(int e = 0; e < n; e++)
        {
          std::cout << "*";
        }

        std::cout << std::endl;
        n = n + 2;
        sc--;
      }
    }
    else
    {
      std::cout << "0 n'est pas un arguments valide" << std::endl;
      return 1;
    }
  }
  else
  {
    if(argc < 2)
    {
      std::cout << "pas d'argument" << std::endl;
    }
    else
    {
      std::cout << "trop argument" << std::endl;
    }
  }

  return 0;
}
